class Development < DeployManDeploymentModule
  def pre_deploy(deployment_hash)
  end

  def deploy(deployment_hash)
    (0..200).each do |i|
      update_output_log("Working: #{i}")
    end

    { success: true }
  end

  def post_deploy(deployment_hash)
  end
end